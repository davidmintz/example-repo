<?php

use PHPUnit\Framework\TestCase;

use ExampleProject\Something;


class TestSomething extends TestCase
{
	
	public function testSomethingInteresting()
	{
		$this->assertTrue(class_exists(Something::class));
	}

}